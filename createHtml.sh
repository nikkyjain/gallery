#!/bin/sh

cat << EOF > index.html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Molly's Photo Gallery</title>
        <link rel="stylesheet" href="js/jquery-mobile/1.5-alpha/css/themes/default/jquery.mobile.css">
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery-mobile/1.5-alpha/jquery.mobile.js"></script>
    </head>
    <style>
        img {
            max-width: 100%;
            max-height: 100%;
            border-radius: 20px 20px 20px 20px;
        }
        #myContainer {
            width: 50%;
            margin: 0 auto;
        }
    </style>
    <script>


        function shuffle(array) {
            for (var i = array.length - 1; i > 0; i--) {
                var j = Math.floor(Math.random() * (i + 1));
                var temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }
        var curIndex=0;
        var nextItem=0;
        var itemNotify=0;
        var photoes = [
EOF

for item in $(ls photoes/*)
do
    myPhoto=$(basename $item)
    if [ "$notFirst" = "1" ]; then
      echo "          ,'photoes/$myPhoto'" >> index.html
    else
      echo "          'photoes/$myPhoto'" >> index.html
    fi
    notFirst=1;
done

cat << EOF >> index.html
        ];
        shuffle(photoes);
        \$( document ).on( 'pagecreate', '#demo-page', function() {
            \$('#myContainer').html('Introducing Molly ..');
            load_image();
            \$('#startStop').click(function () {
              curIndex=photoes.length;
            })
        })

        function load_image() {
            if(curIndex>=photoes.length) {
               console.log("All Done");
               } else if(itemNotify<curIndex) {
                console.log("Not Loaded. curIndex="+curIndex+"itemNotify="+itemNotify);
                setTimeout(load_image, 4000);
                } else {
                    var img = new Image();
                    img.onload = function () {
                        itemNotify=curIndex;
                        \$('#myContainer').html('<img src="'+this.src+'">');
                    }
                    img.src = photoes[curIndex];
                    console.log("Next Image curIndex="+curIndex+" : itemNotify="+itemNotify);
                    curIndex++;
                    setTimeout(load_image, 4000);
                }
        }
    </script>
    <body>
        <div data-role=page id=demo-page>
            <div data-role=header data-position=fixed>
                <div align=center>
                    <h1>Molly
                        <a id=startStop href="#" data-role="button" data-icon="ui-icon-power" data-show-label="false">Start / Stop Slideshow</a>
                    </h1>
                </div>
            </div>
            <div data-role="content" id="content">
                <div id=myContainer> </div>
            </div>
        </div> <!-- Page -->
    </body>
</html>
EOF
